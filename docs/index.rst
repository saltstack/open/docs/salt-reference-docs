===================
Salt Reference Docs
===================

Content will go here

.. toctree::
   :maxdepth: 2
   :hidden:

   topics/extension-test
   See a problem? Open an issue! <https://gitlab.com/saltstack/open/docs/salt-reference-docs/-/issues>
   Salt docs contributing guide <https://saltstack.gitlab.io/open/docs/docs-hub/topics/contributing.html>
   GitLab repository <https://gitlab.com/saltstack/open/docs/salt-reference-docs>
